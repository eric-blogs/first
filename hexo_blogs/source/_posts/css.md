---
title: css layout
date: 2023-08-31 10:26:56
tags:
---

legacy layout
- table
- position
- float

modern css layout
- multi-colum
- flexbox
- grid
## table

## position
normal flow, position is static.

position:
- static （default value）
- relative
- absolute
- fixed
- sticky

ref: https://www.ruanyifeng.com/blog/2019/11/css-position.html
ref: https://developer.mozilla.org/zh-CN/docs/Learn/CSS/CSS_layout/Positioning


## float
- clear
- clear-fixed
- overflow BFC
- display:flow-root;

**issue**

float对line-box和其他元素的影响不一样。

表现：float元素不在normal flow中，其他元素会忽视它，直接占据他的空间

但是line-box会被缩短

不理解why？？？

**浮动补充说明**

浮动的框可以向左或向右移动，直到它的外边缘碰到包含框或另一个浮动框的边框为止。

由于浮动框不在文档的普通流中，所以文档的普通流中的块框表现得就像浮动框不存在一样。

ref: https://www.w3school.com.cn/css/css_positioning_floating.asp


## multi-column

## flexbox

## grid

## others
百分比思想

css 语义和无语义

## css grow
https://blog.51cto.com/u_15057819/3956288
https://rualc.com/frontend/css/#css-jian-jie

## framework

web framework
- React
- vue
- Angular

css framework
- Bootstrap
- Tailwind CSS
- Foundation
- ...
