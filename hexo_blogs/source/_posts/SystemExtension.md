---
title: System Extension Note
date: 2023-11-18 22:20:55
tags: SystemExtension
---

# System Extension Note
## Basic concepts
- Extension: system extension, kernel extension(legacy extension)
- system extension
  - Network Extension (network event)
  - Endpoint Security (file, process and so on event)
  - DriverKit (related to hardware)
  
- rules:
    - Use framewrok to write your system extension and package extension to app bundle
    - Use framewrok to install and update your extension.
    - Once installed, an extension remains available for all users on the system.
    - Disable extension by deleting the app, which deletes the extenson.

- config rules:
    - The extension must match your bundle identifier, excluing file extension.  BundleId:**com.example.usbdriver**, fileName:**com.example.usbdriver**.dext.(bold part is same).
    - same team id for signing extension as signing your app.Unless com.apple.developer.system-extension.redistributable entitlement.
    - Distribute app and extension through Mac App Store , or **Notarize** them.

## Implement drivers, system extensions, and kexts
### Overview
- DriverKit(dext): manage the communication between hardware device and system
- System extension: implement the feature that require kernel-level cooperation, such as custome security and network behavior.
- Kernel extension(kext): supports any low-level services that cannot be implemented using dext or system extension.

Structure
<!-- ![structure image](SystemExtension/se-struct.png) -->
![structure image](se-struct.png)
<!-- {% asset_img se-struct.png "structure image" %} -->

In macOS 11 and later, the kernel doesn’t load a kext if an equivalent System Extension solution exists. 


### Drive Kit
- communicate with custome hardware
    - Apple 提供标准的硬件协议的驱动
    - 自定义的driverkit主要是为了-->特有的feature或者协议
- codeless dext/kext to support custom hardware.
  - A codeless dext, in which your driver class has no implementation.
  - A codeless kext, which has no executable file.
-- 如何开发 ref [doc](https://developer.apple.com/documentation/driverkit/creating_a_driver_using_the_driverkit_sdk?language=objc)

### well-known restrictions for kernel extension
- PACs: pointer authentication codes in arm64e arch. ref [PACs](https://developer.apple.com/documentation/security/preparing_your_app_to_work_with_pointer_authentication?language=objc)

- KIP:  Kernel Integrity Protection. ref [KIP](https://support.apple.com/en-hk/guide/security/sec8b776536b/1/web/1#sec41bf3cd61)

### final step to install kernel extension
If your custom installer package includes kexts, install them as the final installation step. Becuase of restart. ref the [ Installing a Custom Kernel Extension](https://developer.apple.com/documentation/apple-silicon/installing-a-custom-kernel-extension?language=objc)

## Debug and test
### Activate
Activate need 
- proper entitlements
- code signature
- all other criteria for running on the user's system
开发时： 关闭sip之类的验证来加快开发测试的速度

### load path
- 正常运行时: 必须把system extension 放到app bundle的 Contents/Library/SystemExtensions路径下，把app bundle 放到 system's Applications目录下。

- 开发者时：开启developer mode，系统在加载System extension之前并不会检查他的路径，所以可以从任何地方加载System extension。
- 设置develop mode: 
  ``systemextensionsctl developer on/off``
  
### Disable SIP to bypass code-sign and notarization check
In recovery mode, enable/disable SIP by the cmd
`` csrutil enable/disable``

### attach and debug system extesion
lldb
-  ``ps`` 来获取到extension的pid
-  run ``lldb`` with root
-  in lldb, ``process attach --pid``

### Test dext for arm64e
arm64 has PACs issue for kext. To test, do the following:
- Set security level to Medium in recovery mode.
- Disable sip and restart back to macos
- set boot args as **-arm64e_preview_abi**
``sudo nvram boot-args=-arm64e_preview_abi``

## Develop
[Extension active and deactive](https://developer.apple.com/documentation/systemextensions/installing_system_extensions_and_drivers?language=objc)

### Active
During the activation process, the system verifies:
-  Your extension is installed in the Contents/Library/SystemExtensions folder of your app.
-  Your app is installed in an appropriate Applications directory of the system.
-  The code signature of your extension.
-  The entitlements in the code signature match the entitlements granted to your development team.
-  The identifer in your activation request matches the one in your system extension bundle.
-  The identifier isn’t already in use by another system extension.

### Update
skip
### Uninstall
skip
### Examine Installed System Extensions
``systemextensionsctl list``

**Note**
OSSystemExtensionRequestDelegate usage
write related demo



