---
title: Hexo based on gitlab page
date: 2023-08-07 14:39:03
tags:
---

## Background

Want to create a blog for myself but unwilling to spend cost on buying domain and maintain website. So static website is better option.

Firstly, github page is best option for this. Howerver, github is very slow even not access in China mainland. China popular code-hub platform, such as Gitee and coding net (Tencent), do not support page any more. Finding and compare, gitlab is my final choice.

About blog, there are many generators. Such as hexo, jekyll and others. From baidu, jekyll is very powerful and custom at will, but need ruby and not easy.
Hexo is very friendly to new guys and support lots of beautifull themes. Life is short, so, I use hexo ^_^.

In this blog, i will record how to create personal blog with hexo based on gitlab.

## Gitlab page
Compare with github page, which is very easy and maybe got it in 3 mins, git lab page is very headache. Gitlab is base on CI/CD and gitlab page need to config runner. It's big hole, which cost my serval night on it.
### gitlab page
TBD
### gitlab runner
TBD
## Hexo config
TBD
### node
TBD
### docker
TBD
